# Humans vs. Zombies (HvZ)

[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

**Web application** with **Spring Boot** and **Angular**.

## Table of Contents

- [Background](#background)
- [Local running](#localrunning)
- [Usage](#usage)
- [Maintainers](#maintainers)

## Background

Humans vs. Zombies is a web application that was created to demonstrate our experience gained during the 3-month Noroff fullstack developer course.

It is a game of tag. All players begin as humans, and one is randomly chosen to be the “Original Zombie.” The Original Zombie tags human players and turns them into zombies.

## Local running

1. Install:

- Java SDK - https://www.oracle.com/java/technologies/javase/jdk17-archive-downloads.html
- VS Code - https://code.visualstudio.com/
- Node.js and NPM - https://docs.npmjs.com/downloading-and-installing-node-js-and-npm
- PostgreSQL - https://www.postgresql.org/download/

2. Clone the project repositories

- BE - https://gitlab.com/kristof.mata.2/hvz-project-backend.git
- FE - https://gitlab.com/kristof.mata.2/hvz-project-frontend.git

3. Create a postgres database named **hvzdb** and set password **postgres**

4. Install dependencies

- BE - IntelliJ - run **Gradle** to install dependencies
- FE - VS Code - run **npm install** from terminal

5. Run the application

- Backend by the main method from **src/main/java/com/noroff/hvz/HvzApplication.java**
- Frontend by **ng serve** from terminal

6. Local pages

- BE - http://localhost:5000/
- FE - http://localhost:4200/

## Usage

1. Test the application with one of our test users: 
   - as a regular **user** with username: ```user``` password: ```user```
   - as an **admin** with username:```admin``` password: ```admin```
3. You can also register your own user through the app.

## Maintainers

- [Krisztina Pelei](https://gitlab.com/kokriszti)
- [Kristof Mata](https://gitlab.com/kristof.mata.2)
- [Bela Toth](https://gitlab.com/tothbt)
- [Laszlo Laki](https://gitlab.com/lakilukelaszlo)
- [Adam Olah](https://gitlab.com/adam-olah93)
