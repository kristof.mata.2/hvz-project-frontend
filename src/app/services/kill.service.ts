import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Player} from "../models/Player";
import {Kill} from "../models/Kill";
import {CreateKill} from "../models/CreateKill";

@Injectable({
  providedIn: 'root'
})
export class KillService {

  private readonly GAME_URL: string = environment.GAME_URL;

  constructor(private http: HttpClient) { }

  public getAllKillsInGame(gameId: string): Observable<Kill[]> {
    return this.http.get<Kill[]>(`${this.GAME_URL}/${gameId}/kill`);
  }

  public getKillById(gameId: string, killId: string): Observable<Kill> {
    return this.http.get<Kill>(`${this.GAME_URL}/${gameId}/kill/${killId}`);
  }

  public registerKill(kill: CreateKill): Observable<Kill> {
    return this.http.post<Kill>(`${this.GAME_URL}/${kill.game}/kill`, kill)
  }

  public updateKill(gameId: string, kill: Kill): Observable<Kill> {
    return this.http.put<Kill>(`${this.GAME_URL}/${gameId}/kill/${kill.id}`, kill)
  }

  public patchKill(gameId: string, kill: Kill, body: any): Observable<Kill> {
    return this.http.put<Kill>(`${this.GAME_URL}/${gameId}/kill/${kill.id}`, body)
  }

  public deleteKill(gameId: string, killId: string): Observable<Object> {
    return this.http.delete<Object>(`${this.GAME_URL}/${gameId}/kill/${killId}`)
  }
}
