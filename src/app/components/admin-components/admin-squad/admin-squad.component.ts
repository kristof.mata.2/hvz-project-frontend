import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {Game} from "../../../models/Game";

@Component({
  selector: 'app-admin-squad',
  templateUrl: './admin-squad.component.html',
  styleUrls: ['./admin-squad.component.scss']
})
export class AdminSquadComponent implements OnInit, OnChanges {

  @Input() public game?: Game;

  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges(): void {
  }

}
