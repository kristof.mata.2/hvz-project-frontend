import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {Game} from "../../models/Game";
import {Player} from "../../models/Player";

@Component({
  selector: 'app-squad',
  templateUrl: './squad.component.html',
  styleUrls: ['./squad.component.scss']
})
export class SquadComponent implements OnInit, OnChanges {

  @Input() public game?: Game;
  @Input() public player?: Player;

  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges(): void {
    if(this.game && this.player) {

    }
  }

}
