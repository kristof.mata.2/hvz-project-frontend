import { Component, OnInit } from '@angular/core';
import keycloak from "../../../keycloak";
import {LoginUserService} from "../../services/login-user.service";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  constructor(private loginUserService: LoginUserService) { }

  get isLoggedIn(): Boolean | undefined {
    return keycloak.authenticated
  }

  get username(): string | undefined {
    return keycloak.tokenParsed?.preferred_username       //ha encoded info-t akarunk belőle, tokenParsed
  }

  handleLogin(): void {
    keycloak.login();
  }

  handleLogout(): void {
    keycloak.logout()
  }

  ngOnInit(): void {}

}
