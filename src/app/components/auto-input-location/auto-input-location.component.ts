import { Component } from '@angular/core';
import { Options } from 'ngx-google-places-autocomplete/objects/options/options';

@Component({
  selector: 'app-auto-input-location',
  templateUrl: './auto-input-location.component.html',
  styleUrls: ['./auto-input-location.component.scss'],
})
export class AutoInputLocationComponent {
  constructor() {}

  formattedAddress = '';

  options = {
    types: ['(cities)'],
  } as Options;

  public handleAddressChange(address: any) {
    this.formattedAddress = address.vicinity ? address.vicinity : address.name;
    console.log(this.formattedAddress);
  }
}
