import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AutoInputLocationComponent } from './auto-input-location.component';

describe('AutoInputLocationComponent', () => {
  let component: AutoInputLocationComponent;
  let fixture: ComponentFixture<AutoInputLocationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AutoInputLocationComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AutoInputLocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
