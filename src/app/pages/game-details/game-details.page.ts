import { Component, OnDestroy, OnInit } from '@angular/core';
import {Subscription} from "rxjs";
import {Game} from "../../models/Game";
import {ActivatedRoute, Router} from "@angular/router";
import {GameService} from "../../services/game.service";
import {Player} from "../../models/Player";
import {Kill} from "../../models/Kill";
import {Squad} from "../../models/Squad";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {PlayerService} from "../../services/player.service";
import keycloak from "../../../keycloak";
import {LoginUserService} from "../../services/login-user.service";
import {KillService} from "../../services/kill.service";
import {CreateKill} from "../../models/CreateKill";
import {StompService} from "../../services/stomp.service";

@Component({
  selector: 'app-game-details',
  templateUrl: './game-details.page.html',
  styleUrls: ['./game-details.page.scss']
})
export class GameDetailsPage implements OnInit, OnDestroy {

  public gameIdReadFromRoute?: any;
  public playerIdReadFromRoute?: any;
  public routeSubscription?: Subscription;
  public game?: Game;
  public player?: Player;
  public allPlayers?: Player[];
  public allSquads?: Squad[];
  public allKills?: Kill[];
  public wsGameSubscription?: any
  public wsKillSubscription?: any

  public killForm: FormGroup = new FormGroup({
    biteCode: new FormControl("", Validators.required),
    killStory: new FormControl("")
  })

  get username(): string | undefined {
    return keycloak.tokenParsed?.preferred_username
  }

  constructor(private activatedRoute: ActivatedRoute,
              private gameService: GameService,
              private playerService: PlayerService,
              private loginUserService: LoginUserService,
              private killService: KillService,
              private stompService: StompService,
              private router: Router) { }

  ngOnInit(): void {

    //get gameId and -if present - playerId from route
    this.routeSubscription = this.activatedRoute.paramMap.subscribe({
      next: (param) => {
        this.gameIdReadFromRoute = param.get("gameId")
        if(param.get("playerId")) {
          this.playerIdReadFromRoute = param.get("playerId")
        }
      },
      error: (e) => {console.log(e)}
    })    //subs vége

    //load Game
    this.gameService.getGame(this.gameIdReadFromRoute).subscribe({
      next: (game) => {this.game = game},
      error: (e) => {console.log(e)}
    })

    //get PLayer list
    this.loginUserService.getLoginUser(keycloak.tokenParsed?.sub).subscribe({
      next: (playersFromServer: Player[]) => {
        console.log(playersFromServer)
        //ha van playerID úvonalban
        if(this.playerIdReadFromRoute) {
          //de DB szerint nem Player, vagy Player, de az útvonalban más id van
          if(!this.getPlayerId(playersFromServer) || (this.getPlayerId(playersFromServer) != this.playerIdReadFromRoute) ) {
            this.router.navigateByUrl(`game/${this.gameIdReadFromRoute}`)           //gameId útvonalra
          //DB szerint is Player, állítsuk be a Playert
          } else {
            // this.player = playersFromServer.find(player => this.game?.id == player?.game)
            this.player = playersFromServer.find(player => player.id == this.getPlayerId(playersFromServer))
          }
        //ha nincs playerID útvonalban
        } else {
          //de DB szerint van PlayerId-ja ehhez a game-hez
          if(this.getPlayerId(playersFromServer)) {
            this.router.navigateByUrl(`game/${this.gameIdReadFromRoute}/${this.getPlayerId(playersFromServer)}`)
          }
        }
      },
      //if User is not in DB
      error: (e) => {console.log(e)}
    })

    setTimeout(() => {
//websocket subscription to Game updates
      this.wsGameSubscription = this.stompService.subscribe(`/topic/game/${this.gameIdReadFromRoute}`, (response: any):void => {
        console.log("notified")
        console.log(response.body)

        this.refreshGame();
        if(this.game && this.player?.id) {
          this.playerService.getPlayerById(this.game?.id, this.player?.id).subscribe({
            next: (player: Player) => {
              this.player = player
              if(response.body == "update_game_start") {
                alert(`Game started! You ${this.player.isPatientZero ? "are" : "are not"} Patient Zero`)
              }
              if(response.body == "update_game_end") {
                alert("Game over")
              }
            },
            error: (e) => {console.log(e)}
          })
        }

      })

      //websocket subscription to Kill
      this.wsKillSubscription = this.stompService.subscribe(`/topic/kill/${this.playerIdReadFromRoute}`, (response: any):void => {
        console.log("notified")
        console.log(response.body)

        this.refreshGame();

        this.refreshPlayer()
        alert("You've been bitten! You are now a Zombie!")

      })
    }, 1000)


  }

  onKillSubmit() {
    const newKill: CreateKill = {
      time: new Date(Date.now()),
      location: "some coordinates",
      killerId: this.player?.id,
      biteCode: this.killForm.get("biteCode")?.value,
      game: this.game?.id
    }
    console.log(newKill)

    this.killService.registerKill(newKill).subscribe({
      next: () => {
        console.log("kill registered")
        this.killForm.reset()
      },
      error: (e) => {console.log(e)}
    })
  }

  joinGame() {
    const newPlayer: Player = {
      isHuman: true,
      isPatientZero: false,
      game: this.game?.id,
      keycloakId: keycloak.tokenParsed?.sub
    }

    this.playerService.addPlayerToGame(this.gameIdReadFromRoute, newPlayer).subscribe({
      next: (response) => {
        console.log("Player added")
        const locationFromHeaders = response.headers.get('Location').split("/");
        const playerId = locationFromHeaders[locationFromHeaders.length-1]
        this.router.navigateByUrl(`game/${this.gameIdReadFromRoute}/${playerId}`)
      },
      error: (e) => {console.log(e)}
    })

  }

  leaveGame() {
    this.playerService.deletePalyer(this.gameIdReadFromRoute, this.playerIdReadFromRoute).subscribe({
      next: () => {
        console.log("Player deleted")
      },
      error: (e) => {console.log(e)}
    })
    this.router.navigateByUrl(`game/${this.gameIdReadFromRoute}`)
  }

  public getPlayerId(playersOfUser: Player[]): number |undefined {
    let playerId = undefined;
    playersOfUser.forEach(player => {
      if (this.game?.id == player?.game) {
        playerId = player.id;
      }
    })
    return playerId;
  }

  public refreshGame() {
    if (this.game) {
      this.gameService.getGame(this.game?.id).subscribe({
        next: (game: Game) => {
          this.game = game;
          console.log(game)
        },
        error: (e) => {
          console.log(e)
        }
      })
    }
  }

  public refreshPlayer() {
    if(this.game && this.player?.id) {
      this.playerService.getPlayerById(this.game?.id, this.player?.id).subscribe({
        next: (player: Player) => {
          this.player = player
        },
        error: (e) => {console.log(e)}
      })
    }

  }

  // ngClass: CSS classes added/removed per current state of game
  setCurrentClasses(game: Game | undefined): Record<string, boolean> {
    if(game) {
      return {
        'game-state': true,
        'game-state-reg': game.state === 'Registration',
        'game-state-in-prog': game.state === 'In Progress',
        'game-state-compl': game.state === 'Complete'
      };
    } else {
      return {
        'game-state': true
      };
    }
  }

  ngOnDestroy(): void {
    if(this.routeSubscription) {
      this.routeSubscription.unsubscribe();
    }
    if(this.wsGameSubscription) {
      this.stompService.unsubscribeFromTopic(this.wsGameSubscription);
    }
    if(this.wsKillSubscription) {
      this.stompService.unsubscribeFromTopic(this.wsKillSubscription);
    }
  }


}
