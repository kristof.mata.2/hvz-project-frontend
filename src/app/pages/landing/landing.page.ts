import { Component, OnInit } from '@angular/core';
import keycloak from "../../../keycloak";

@Component({
  selector: 'app-landing',
  templateUrl: './landing.page.html',
  styleUrls: ['./landing.page.scss']
})
export class LandingPage implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  get username(): string | undefined {
    return keycloak.tokenParsed?.preferred_username       //ha encoded info-t akarunk belőle, tokenParsed
  }

  get isLoggedIn(): Boolean | undefined {
    return keycloak.authenticated
  }

}
