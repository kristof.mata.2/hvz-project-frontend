import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {GameService} from "../../services/game.service";
import {Subscription} from "rxjs";
import {Game} from "../../models/Game";
import {Player} from "../../models/Player";
import {PlayerService} from "../../services/player.service";
import {StompService} from "../../services/stomp.service";
import keycloak from "../../../keycloak";

@Component({
  selector: 'app-admin',
  templateUrl: './admin.page.html',
  styleUrls: ['./admin.page.scss']
})
export class AdminPage implements OnInit, OnDestroy {

  public gameIdReadFromRoute?: any;
  public routeSubscription?: Subscription;
  public game?: Game;
  public players?: Player[];
  public wsGameSubscription?: any
  public wsKillSubscription?: any

  get username(): string | undefined {
    return keycloak.tokenParsed?.preferred_username
  }

  constructor(private activatedRoute: ActivatedRoute,
              private gameService: GameService,
              private playerService: PlayerService,
              private stompService: StompService
              ) { }

  ngOnInit(): void {

    this.routeSubscription = this.activatedRoute.paramMap.subscribe({
      next: (param) => {
        this.gameIdReadFromRoute = param.get("gameId")
      },
      error: (e) => {console.log(e)}
    })    //subs vége

    //load Game
    this.gameService.getGame(this.gameIdReadFromRoute).subscribe({
      next: (game) => {this.game = game},
      error: (e) => {console.log(e)}
    })

    //load Players
    this.playerService.getAllPlayersInGame(this.gameIdReadFromRoute).subscribe({
      next: (players) => {
        this.players = players
        console.log(this.players)
      },
      error: (e) => {console.log(e)}
    })

    setTimeout(() => {
//websocket subscription to Game updates
      this.wsGameSubscription = this.stompService.subscribe(`/topic/game/${this.gameIdReadFromRoute}`, (response: any):void => {
        console.log("notified")
        console.log(response.body)

        this.refreshGame();
        this.refreshPlayers();
      })

      //websocket subscription to Kill
      this.wsKillSubscription = this.stompService.subscribe(`/topic/kill/${this.gameIdReadFromRoute}`, (response: any):void => {
        console.log("notified")
        console.log(response.body)

        this.refreshPlayers();

      })

    }, 1000)

  }

  public changeGameState(state: string): void {
    if(this.game) {
      this.gameService.patchGame(this.game.id, {state: state}).subscribe({
        next: () => {
          console.log("Game state changed to " + state)
        },
        error: (e) => {console.log(e)}
      })
    }
  }

  public refreshGame() {
    if (this.game) {
      this.gameService.getGame(this.game?.id).subscribe({
        next: (game: Game) => {
          this.game = game;
          console.log(game)
        },
        error: (e) => {
          console.log(e)
        }
      })
    }
  }

  public refreshPlayers() {
    if (this.game) {
      this.playerService.getAllPlayersInGame(this.gameIdReadFromRoute).subscribe({
        next: (players) => {
          this.players = players
        },
        error: (e) => {console.log(e)}
      })
    }
  }

  // ngClass: CSS classes added/removed per current state of game
  setCurrentClasses(game: Game | undefined): Record<string, boolean> {
    if(game) {
      return {
        'game-state': true,
        'game-state-reg': game.state === 'Registration',
        'game-state-in-prog': game.state === 'In Progress',
        'game-state-compl': game.state === 'Complete'
      };
    } else {
      return {
        'game-state': true
      };
    }
  }

  ngOnDestroy(): void {
    if(this.routeSubscription) {
      this.routeSubscription.unsubscribe();
    }
    if(this.wsGameSubscription) {
      this.stompService.unsubscribeFromTopic(this.wsGameSubscription);
    }
    if(this.wsKillSubscription) {
      this.stompService.unsubscribeFromTopic(this.wsKillSubscription);
    }
  }

}
