import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import { Observable } from 'rxjs';
import keycloak from "../../keycloak";

@Injectable({
  providedIn: 'root'
})
export class RoleGuard implements CanActivate {

  constructor(private router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    // Authenticated
    if (keycloak.authenticated && keycloak.hasResourceRole("admin")) {
      return true;
    }

    // Automatically navigate to landing page
    this.router.navigateByUrl("");
    return false;
  }

}
